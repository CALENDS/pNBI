from mpdaf.obj import Cube,Image,Spectrum,iter_ima #Iter_ima va permettre de parcourir tous les plans(longueur d'onde) les un apres les autres
import numpy as np
#import read_master_table as rmt
#import read_ray_wave as rrw
import os
import optparse
import sys
import scipy

def nanmad(arr):   # It is not used yet
    """ Median Absolute Deviation: a "Robust" version of standard deviation.
        Indices variabililty of the sample.
        https://en.wikipedia.org/wiki/Median_absolute_deviation 
    """
    arr = np.ma.array(arr).compressed() # should be faster to not use masked arrays.
    arr = arr[np.isfinite(arr)]
    med = np.median(arr)

    return np.median(np.abs(arr - med))

def mad(arr):
    """ Median Absolute Deviation: a "Robust" version of standard deviation.
        Indices variabililty of the sample.
        https://en.wikipedia.org/wiki/Median_absolute_deviation 
    """
    arr = np.ma.array(arr).compressed() # should be faster to not use masked arrays.
    med = np.median(arr)

    return np.median(np.abs(arr - med))


def make_minicube(c,DEC,RA,arcsec_size,ray_lmd,agstr_window=110):
	minic=c.subcube((DEC,RA),arcsec_size,lbda=(ray_lmd-agstr_window/2.0,ray_lmd+agstr_window/2.0))
	return minic
	
def make_continuum_free_cube(mc,size_box=5):
	mc_res=mc.copy()
	pixmidlambda=len(mc.data.data[:,0,0])/2
	for yy in range(0,len(mc.data.data[0,:,0])):
        	for xx in range(0,len(mc.data.data[0,0,:])):
                	#avepix=np.mean([np.mean(mc.data.data[:pixmidlambda-size_box,yy,xx]),np.mean(mc.data.data[pixmidlambda+size_box:,yy,xx])  ])  #DO the mean of the mean
                	#med_pix=np.mean([np.median(mc.data.data[:pixmidlambda-size_box,yy,xx]),np.median(mc.data.data[pixmidlambda+size_box:,yy,xx])  ])  #DO the mean of the median
                	#mad_pix=mad(np.concatenate((mc.data.data[:pixmidlambda-size_box,yy,xx],mc.data.data[pixmidlambda+size_box:,yy,xx])))              
			sgmcl_pix=np.mean(scipy.stats.sigmaclip(np.concatenate((mc.data.data[:pixmidlambda-size_box,yy,xx],mc.data.data[pixmidlambda+size_box:,yy,xx])),7,7)[0]) #sigma clipping
			#mc_res.data.data[:,yy,xx]=mc.data.data[:,yy,xx]-avepix
			#mc_res.data.data[:,yy,xx]=mc.data.data[:,yy,xx]-mad_pix
			#mc_res.data.data[:,yy,xx]=mc.data.data[:,yy,xx]-med_pix
			mc_res.data.data[:,yy,xx]=mc.data.data[:,yy,xx]-sgmcl_pix
	return mc_res


def get_first_sigma(im):
        return 1.4855*mad(im.data.data)

#def create_table_from_list(parameter_filename,agstr_window_default,size_box_default,lmbda_range_default,space_window_default):
def create_table_from_list(parameter_filename,agstr_window_preset,size_box_preset,lmbda_range_preset,space_window_preset):
        """
        Reads and parses the parameter file 
        
        Parameters
        ----------
        parameter_filename : string     
                             Name of the parameter file
        [default_values]   : several default values that can be used if no options was set
        Returns
        ----------
        cube_path,other_param : string,list of list
                        Returns the path of the cube and the list of parameters 
        """
        #a=[]
	full_line=[]
	param_tab=[]
        #b=[]
        #comments=[]
        #still_recording=False
	#c,RA,DEC,wavelength,space_window,agstr_window,lmbda_range,size_box,NB_outname
	f=open(parameter_filename, 'r')
        cube_path = f.readline().strip().split()
	print cube_path
        #data_rows=[]
        for line in f:
		space_window=space_window_preset
		agstr_window=agstr_window_preset
		lmbda_range=lmbda_range_preset
		size_box=size_box_preset
		print 'line no strip',line
        	line = line.strip()
		print 'line strip',line
                columns = line.split()
		print 'columns',columns
		NB_outname='NB_'+columns[0]+'.fits'
		RA=columns[1]
		DEC=columns[2]
		#wavelength=columns[4]
		full_line.append(RA)
		full_line.append(DEC)
		print 'start full_line',full_line
		for i in range(0,len(columns)):
			if columns[i]=='-l':
				wavelength=float(columns[i+1])
			if columns[i]=='-f':
				space_window=float(columns[i+1])
			if columns[i]=='-s':
				agstr_window=float(columns[i+1])
			if columns[i]=='-w':
				lmbda_range=float(columns[i+1])
			if columns[i]=='-m':
				size_box=float(columns[i+1])
			
		full_line.append(wavelength)
		full_line.append(space_window)
		full_line.append(agstr_window)
		full_line.append(lmbda_range)
		full_line.append(size_box)
		full_line.append(NB_outname)
		print 'full_line',full_line
		param_tab.append(full_line)
		full_line=[]

        return cube_path[0],param_tab




def process_cube_to_get_narrow_band_images_of_multiple(c,RA,DEC,wavelength,space_window,ag_win,lmbda_range,size_box,NB_outname):
	minic=make_minicube(c,DEC,RA,space_window,wavelength,agstr_window=ag_win)
	mc_wc=make_continuum_free_cube(minic,size_box)		
	mc_narrow=make_minicube(mc_wc,DEC,RA,space_window,wavelength,agstr_window=lmbda_range)
        
        im=mc_narrow.sum(axis=0)
	print 'the first sigma level according to a mad statistic (1.4855*mad) of the picture is',get_first_sigma(im)
        im.write(NB_outname)
        
	test=mc_wc.subcube((DEC,RA),size=8)   # I male spectra from a smaller region
        spec=test.sum(axis=(1,2))
	np.savetxt('spec_output.txt',zip(test.wave.coord(),spec.data.data))
        


if __name__== "__main__":
	

        print '   ^                                            /\\'
        print '   |                                           /  \\'
        print '   |                                          /    \\'
        print '   | _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ /      \_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _'
        print '   |___________________________________________________________________________________________>'
        print '                                          |    -w     |'
        print '                                      ||       -m        ||'
        print '                            |||                -s                |||'
        print ' '
        print ' '
        print ' '
        print '         ...............................|                 |...................................'
        print '         |-- wavelength range collasped to form the NB image  key: -w   Default value: 10  --|'
        print ' '
        print '     ............................||                             ||...............................'
        print '     ||------------- Masked value used on the continuum range  key: -m   Default value: 10-----||'
        print ' '
        print '...................|||                                                     |||...........................'
	print '|||----------------   Continuum range   key: -s    Default value: 110  -------------------------------|||'
	
	
	agstr_window_default=110
	size_box_default=5
	lmbda_range_default=10
	NB_outname_default='narrowbandimages_output.fits'
        space_window_default=20.0
	

	parser = optparse.OptionParser()
	parser.add_option('-s', '--spectral_continuum_range', dest='spec_range', help='spectral range where you will have the continuum suroundings',type=float)
	parser.add_option('-f', '--spatial_fov', dest='fov', help='spatial window in arcsec, value correspond to the delta axis and image is a square',type=float)
	parser.add_option('-w', '--wavelength_window', dest='wave_win', help='wavelength window, full range centered on the line selected',type=float)
	parser.add_option('-l', '--lambda', dest='lmbda', help='lambda',type=float)
	parser.add_option('-c', '--cube', dest='cube', help='cube')
	parser.add_option('-o', '--output', dest='name', help='output name of the narrow_band images')
	parser.add_option('-m', '--mask_continuum', dest='mask', help='size of the mask in pixel for the central part of the spectrum  ||continuum | mask | continuum ||',type=float)
	parser.add_option('-L', '--list_param', dest='list_param', help='file which containt the list of command. First line is the path of the cube and other line are at least ID RA DEC -l lambda_center_value. In each line other option could be added after (only short version)')
	(options, args) = parser.parse_args()

	


	#cube_default_path='/data/guillaume/A2744_data/ZAP/DATACUBE_A2744_ZAP_1Jan_MAD_ZAP_median.fits'
	cube_default_path='/data/guillaume/A2744_data/ZAP/DATACUBE_A2744_ZAP_MAD_ZAP_Median.fits'
	#cube_default_path='/data/guillaume/A2744_data/CFCS/DATACUBE_A2744_CubeFixSharp_20Dec_1_MAD.fits'

	if options.spec_range is None:
		agstr_window=agstr_window_default
		print 'default spectral range =',agstr_window
	else:
		agstr_window=options.spec_range
		print 'user define spectral range =',agstr_window
	if options.mask is None:
		size_box=size_box_default
	else:
		size_box=options.mask/2.0
	if options.wave_win is None:
		lmbda_range=lmbda_range_default
	        print 'default wavelength_window = ',lmbda_range
	else:
		lmbda_range=options.wave_win
	        print 'user set wavelength_window =',lmbda_range



	if options.name is None:
		NB_outname=NB_outname_default
	else :
		NB_outname=options.name

	if options.fov is None:
                space_window=space_window_default
        else :
                space_window=options.fov

	if options.list_param is None :   # If I do not manage with list but only with single object
	   ##Cube
	   if options.cube is None :
		try:
			c=Cube(cube_default_path)
		except:
			options.cube=raw_input('Enter Cube path:')
			c=Cube(str(options.cube))
	   else:
		c=Cube(str(options.cube))
	   ### RA DEC
	   try:
		RADEC=sys.argv[1]
		RA=np.float(RADEC.split(',')[0])
		DEC=np.float(RADEC.split(',')[1])
	   except:
		RADEC=str(raw_input('Enter RA,DEC:'))
		RA=np.float(RADEC.split(',')[0])
                DEC=np.float(RADEC.split(',')[1])
	   ###Lambda
	   if options.lmbda is None:
		options.lmbda= raw_input('Enter lambda:')
		wavelength=float(options.lmbda)
	   else:	
		wavelength=options.lmbda
		print 'option lamba= ',wavelength
	else :                             # If the option using a list is set
		print 'options.list_param',options.list_param
		cube_path,list_param=create_table_from_list(options.list_param,agstr_window,size_box,lmbda_range,space_window)
		#list_param=np.genfromtxt(str(options.list_param),dtype='str')
		#RA_l=list_param[:,1]
		#DEC_l=list_param[:,2]
		#lmbda_l=list_param[:,np.argmax(list_param=='-l')+1].astype(float)
                #print list_param
		print 'cube_path',cube_path
		c=Cube(cube_path)
	

	if options.list_param is None:

		print size_box
		process_cube_to_get_narrow_band_images_of_multiple(c,RA,DEC,wavelength,space_window,agstr_window,lmbda_range,size_box,NB_outname)
	else :
		for line in list_param :
			RA=line[0]
			DEC=line[1]
			wavelength=line[2]
			space_window=line[3]
			agstr_window=line[4]
			lmbda_range=line[5]
			size_box=line[6]
			NB_outname=line[7]
			print RA,DEC,wavelength,space_window,agstr_window,lmbda_range,size_box,NB_outname
			process_cube_to_get_narrow_band_images_of_multiple(c,RA,DEC,wavelength,space_window,agstr_window,lmbda_range,size_box,NB_outname)
	

